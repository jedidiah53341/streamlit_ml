import streamlit as st
import pandas as pd
import numpy as np

st.title('ML Simple Modelling')
st.caption('Upload a .csv file on the sidebar, then pick the method you want to try.')

mlAlgorithm= ''

uploaded_file = st.sidebar.file_uploader("Choose a file")
if uploaded_file is not None:
     df = pd.read_csv(uploaded_file)
     st.subheader("Data header")
     st.write(df.head())
     st.subheader("Descriptive statistics")
     st.write(df.describe())
     st.sidebar.subheader("Columns with null values")
     st.sidebar.write(df.isna().sum())
     mlAlgorithm = st.selectbox(
     'Choose the machine learning algorithm.',
     ('Choose',
     'Regression - Multiple Linear Regression',
     'Regression - Decision Tree',
     'Regression - Support Vector Regression',
     'Regression - Lasso Regression',
     'Classification - Binomial Logistic Regression',
     'Classification - Random Forest',
     'Classification - Gradient Boosting',
     'Classification - Gaussian Naive Bayes',
     'Classification - Support Vector Machines',
     'Classification - K-nearest Neighbors',))


if mlAlgorithm == 'Regression - Multiple Linear Regression':
     st.subheader("1. Choosing independent and dependent variables")
     col1, col2 = st.columns(2)
     with col1:
              independentVariables = st.multiselect(
     'Which columns do you want to set as the independent values(X) ?',
     list(df.columns))
     st.write('The independent variables are:', independentVariables)
     with col2:
              dependentVariables = st.selectbox(
                  'Which column do you want to set as the dependent value(y)?',
                  list(df.columns))
              X = df[independentVariables]
              y = df[dependentVariables]

    

     st.subheader("2. Determining Size of Train Dataset")
     # importing train_test_split from sklearn
     from sklearn.model_selection import train_test_split
     # splitting the data
     testSize = st.slider(label='Set the test size here..', min_value=0.0, max_value=1.0,value=0.2)
     X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = testSize)

     trainCol1, trainCol2, trainCol3, trainCol4 = st.columns(4)
     with trainCol1:
          st.write('X_train size:', len(X_train), " rows")
          st.write('X_train:', X_train)
     with trainCol2:
          st.write('X_test size:', len(X_test), " rows")
          st.write('X_test:', X_test)
     with trainCol3:
          st.write('y_train size:', len(y_train), " rows")
          st.write('y_train:', y_train)
     with trainCol4:
          st.write('y_test size:', len(y_test), " rows")
          st.write('y_test:', y_test)
          
     # importing module
     from sklearn.linear_model import LinearRegression
     # creating an object of LinearRegression class
     LR = LinearRegression()
     # fitting the training data
     LR.fit(X_train,y_train)
     y_prediction =  LR.predict(X_test)
     # importing r2_score module
     from sklearn.metrics import r2_score
     from sklearn.metrics import mean_squared_error
     # predicting the accuracy score
     score=r2_score(y_test,y_prediction)
     st.subheader("3. Accuracy Scores")
     st.write('An R2 score higher than 0.8 is generally good, yet sometimes the standard can go as high as 0.9 to be considered excellent.')
     st.write('R2 score is ',score)
     st.write('mean_sqrd_error is=',mean_squared_error(y_test,y_prediction))
     st.write('root_mean_squared error of is=',np.sqrt(mean_squared_error(y_test,y_prediction)))

     # Predicting new values
     st.subheader("You can try to use the model to predict new values here")
     toPredict = st.text_input('Insert values of independent-variables, separate them by commas.')

     indepentVariablesValues = []
     toPredictValue = toPredict.split(",")
     for value in toPredictValue:
          indepentVariablesValues.append(int(value))
     st.write(classifier.predict([indepentVariablesValues]))

     # Save the model
     if st.button('Save Model'):
         import joblib
         joblib.dump(LR, 'saved_model_linear_regression.pkl')
     
if mlAlgorithm == 'Classification - Binomial Logistic Regression':

     from sklearn.preprocessing import LabelEncoder
     for column in df.columns:
          if df[column].dtype == np.number: continue
          # Perform encoding for each non-numeric column
          df[column] = LabelEncoder().fit_transform(df[column])
     st.write("All the non-numeric columns have been converted to numerical data types")
     st.write(df.describe())
          
     st.subheader("1. Choosing independent and dependent variables")
     col1, col2 = st.columns(2)
     with col1:
              independentVariables = st.multiselect(
     'Which columns do you want to set as the independent values(X) ?',
     list(df.columns))
     st.write('The independent variables are:', independentVariables)
     with col2:
              dependentVariables = st.selectbox(
                  'Which column do you want to set as the dependent value(y)?',
                  list(df.columns))
              X = df[independentVariables]
              y = df[dependentVariables]

          
     st.subheader("2. Determining Size of Train Dataset")
     # importing train_test_split from sklearn
     from sklearn.model_selection import train_test_split
     # splitting the data
     testSize = st.slider(label='Set the test size here..', min_value=0.0, max_value=1.0,value=0.2)
     X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = testSize)

     trainCol1, trainCol2, trainCol3, trainCol4 = st.columns(4)
     with trainCol1:
          st.write('X_train size:', len(X_train), " rows")
          st.write('X_train:', X_train)
     with trainCol2:
          st.write('X_test size:', len(X_test), " rows")
          st.write('X_test:', X_test)
     with trainCol3:
          st.write('y_train size:', len(y_train), " rows")
          st.write('y_train:', y_train)
     with trainCol4:
          st.write('y_test size:', len(y_test), " rows")
          st.write('y_test:', y_test)
          
     # Feature Scaling
     from sklearn.preprocessing import StandardScaler
     sc_X = StandardScaler()
     X_train = sc_X.fit_transform(X_train)
     X_test = sc_X.transform(X_test)

     # Fitting Logistic Regression to the Training Set
     from sklearn.linear_model import LogisticRegression
     classifier = LogisticRegression(random_state=0)
     classifier.fit(X_train, y_train)

     # Predicting the Test set results
     y_pred = classifier.predict(X_test)

     st.subheader("3. Accuracy Scores")
     #Making the Confusion Matrix
     from sklearn.metrics import confusion_matrix, classification_report
     cm = confusion_matrix(y_test, y_pred)
     st.subheader("Confusion Matrix for Logistic Regression")
     st.write(cm[0][0], " true negative predictions.")
     st.write(cm[1][0], " false negative predictions.")
     st.write(cm[1][0], " false positive predictions.")
     st.write(cm[1][1], " true positive predictions.")

     # Calculating Accuracy
     accuracyScore = ( cm[0][0] + cm[1][1] ) / ( len(y_test) )
     st.write("Accuracy Score:", accuracyScore)

     # Classification Report
     st.subheader("Classification Report for Logistic Regression")
     st.write(classification_report(y_test, y_pred))

     # Predicting new values
     st.subheader("You can try to use the model to predict new values here")
     toPredict = st.text_input('Insert values of independent-variables, separate them by commas.')

     indepentVariablesValues = []
     toPredictValue = toPredict.split(",")
     for value in toPredictValue:
          indepentVariablesValues.append(int(value))
     st.write(classifier.predict([indepentVariablesValues]))

     # Save the model
     if st.button('Save Model'):
          import joblib
          joblib.dump(classifier, 'saved_model_binomial_logistic_regression.pkl')

if mlAlgorithm == 'Classification - Random Forest':

     from sklearn.preprocessing import LabelEncoder
     for column in df.columns:
          if df[column].dtype == np.number: continue
          # Perform encoding for each non-numeric column
          df[column] = LabelEncoder().fit_transform(df[column])
     st.write("All the non-numeric columns have been converted to numerical data types")
     st.write(df.describe())
          
     st.subheader("1. Choosing independent and dependent variables")
     col1, col2 = st.columns(2)
     with col1:
              independentVariables = st.multiselect(
     'Which columns do you want to set as the independent values(X) ?',
     list(df.columns))
     st.write('The independent variables are:', independentVariables)
     with col2:
              dependentVariables = st.selectbox(
                  'Which column do you want to set as the dependent value(y)?',
                  list(df.columns))
              X = df[independentVariables]
              y = df[dependentVariables]

          
     st.subheader("2. Determining Size of Train Dataset")
     # importing train_test_split from sklearn
     from sklearn.model_selection import train_test_split
     # splitting the data
     testSize = st.slider(label='Set the test size here..', min_value=0.0, max_value=1.0,value=0.2)
     X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = testSize)

     trainCol1, trainCol2, trainCol3, trainCol4 = st.columns(4)
     with trainCol1:
          st.write('X_train size:', len(X_train), " rows")
          st.write('X_train:', X_train)
     with trainCol2:
          st.write('X_test size:', len(X_test), " rows")
          st.write('X_test:', X_test)
     with trainCol3:
          st.write('y_train size:', len(y_train), " rows")
          st.write('y_train:', y_train)
     with trainCol4:
          st.write('y_test size:', len(y_test), " rows")
          st.write('y_test:', y_test)
          
     # Feature Scaling
     from sklearn.preprocessing import StandardScaler
     sc_X = StandardScaler()
     X_train = sc_X.fit_transform(X_train)
     X_test = sc_X.transform(X_test)

     # Fitting Random Forest Classifier to the Training Set
     from sklearn.ensemble import RandomForestClassifier
     classifier = RandomForestClassifier()
     classifier.fit(X_train, y_train)

     # Predicting the Test set results
     y_pred = classifier.predict(X_test)

     st.subheader("3. Accuracy Scores")
     #Making the Confusion Matrix
     from sklearn.metrics import confusion_matrix, classification_report
     cm = confusion_matrix(y_test, y_pred)
     st.subheader("Confusion Matrix for Random Forest Classifier")
     st.write(cm[0][0], " true negative predictions.")
     st.write(cm[1][0], " false negative predictions.")
     st.write(cm[1][0], " false positive predictions.")
     st.write(cm[1][1], " true positive predictions.")

     # Calculating Accuracy
     accuracyScore = ( cm[0][0] + cm[1][1] ) / ( len(y_test) )
     st.write("Accuracy Score:", accuracyScore)

     # Classification Report
     st.subheader("Classification Report for Random Forest Classifier")
     st.write(classification_report(y_test, y_pred))

     # Predicting new values
     st.subheader("You can try to use the model to predict new values here")
     toPredict = st.text_input('Insert values of independent-variables, separate them by commas.')

     indepentVariablesValues = []
     toPredictValue = toPredict.split(",")
     for value in toPredictValue:
          indepentVariablesValues.append(int(value))
     st.write(classifier.predict([indepentVariablesValues]))
     
     # Save the model
     if st.button('Save Model'):
          import joblib
          joblib.dump(classifier, 'saved_model_random_forest_classifier.pkl')

if mlAlgorithm == 'Classification - Gradient Boosting':

     from sklearn.preprocessing import LabelEncoder
     for column in df.columns:
          if df[column].dtype == np.number: continue
          # Perform encoding for each non-numeric column
          df[column] = LabelEncoder().fit_transform(df[column])
     st.write("All the non-numeric columns have been converted to numerical data types")
     st.write(df.describe())
          
     st.subheader("1. Choosing independent and dependent variables")
     col1, col2 = st.columns(2)
     with col1:
              independentVariables = st.multiselect(
     'Which columns do you want to set as the independent values(X) ?',
     list(df.columns))
     st.write('The independent variables are:', independentVariables)
     with col2:
              dependentVariables = st.selectbox(
                  'Which column do you want to set as the dependent value(y)?',
                  list(df.columns))
              X = df[independentVariables]
              y = df[dependentVariables]

          
     st.subheader("2. Determining Size of Train Dataset")
     # importing train_test_split from sklearn
     from sklearn.model_selection import train_test_split
     # splitting the data
     testSize = st.slider(label='Set the test size here..', min_value=0.0, max_value=1.0,value=0.2)
     X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = testSize)

     trainCol1, trainCol2, trainCol3, trainCol4 = st.columns(4)
     with trainCol1:
          st.write('X_train size:', len(X_train), " rows")
          st.write('X_train:', X_train)
     with trainCol2:
          st.write('X_test size:', len(X_test), " rows")
          st.write('X_test:', X_test)
     with trainCol3:
          st.write('y_train size:', len(y_train), " rows")
          st.write('y_train:', y_train)
     with trainCol4:
          st.write('y_test size:', len(y_test), " rows")
          st.write('y_test:', y_test)
          
     # Feature Scaling
     from sklearn.preprocessing import StandardScaler
     sc_X = StandardScaler()
     X_train = sc_X.fit_transform(X_train)
     X_test = sc_X.transform(X_test)

     # Fitting Gradient Boosting Classifier to the Training Set
     from sklearn.ensemble import GradientBoostingClassifier
     classifier = GradientBoostingClassifier()
     classifier.fit(X_train, y_train)

     # Predicting the Test set results
     y_pred = classifier.predict(X_test)

     st.subheader("3. Accuracy Scores")
     #Making the Confusion Matrix
     from sklearn.metrics import confusion_matrix, classification_report
     cm = confusion_matrix(y_test, y_pred)
     st.subheader("Confusion Matrix for Gradient Boosting Classifier")
     st.write(cm[0][0], " true negative predictions.")
     st.write(cm[1][0], " false negative predictions.")
     st.write(cm[1][0], " false positive predictions.")
     st.write(cm[1][1], " true positive predictions.")

     # Calculating Accuracy
     accuracyScore = ( cm[0][0] + cm[1][1] ) / ( len(y_test) )
     st.write("Accuracy Score:", accuracyScore)

     # Classification Report
     st.subheader("Classification Report for Gradient Boosting Classifier")
     st.write(classification_report(y_test, y_pred))

     # Predicting new values
     st.subheader("You can try to use the model to predict new values here")
     toPredict = st.text_input('Insert values of independent-variables, separate them by commas.')

     indepentVariablesValues = []
     toPredictValue = toPredict.split(",")
     for value in toPredictValue:
          indepentVariablesValues.append(int(value))
     st.write(classifier.predict([indepentVariablesValues]))
     
     # Save the model
     if st.button('Save Model'):
          import joblib
          joblib.dump(classifier, 'saved_model_gradient_boosting_classifier.pkl')
          
if mlAlgorithm == 'Classification - Gaussian Naive Bayes':

     from sklearn.preprocessing import LabelEncoder
     for column in df.columns:
          if df[column].dtype == np.number: continue
          # Perform encoding for each non-numeric column
          df[column] = LabelEncoder().fit_transform(df[column])
     st.write("All the non-numeric columns have been converted to numerical data types")
     st.write(df.describe())
          
     st.subheader("1. Choosing independent and dependent variables")
     col1, col2 = st.columns(2)
     with col1:
              independentVariables = st.multiselect(
     'Which columns do you want to set as the independent values(X) ?',
     list(df.columns))
     st.write('The independent variables are:', independentVariables)
     with col2:
              dependentVariables = st.selectbox(
                  'Which column do you want to set as the dependent value(y)?',
                  list(df.columns))
              X = df[independentVariables]
              y = df[dependentVariables]

          
     st.subheader("2. Determining Size of Train Dataset")
     # importing train_test_split from sklearn
     from sklearn.model_selection import train_test_split
     # splitting the data
     testSize = st.slider(label='Set the test size here..', min_value=0.0, max_value=1.0,value=0.2)
     X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = testSize)

     trainCol1, trainCol2, trainCol3, trainCol4 = st.columns(4)
     with trainCol1:
          st.write('X_train size:', len(X_train), " rows")
          st.write('X_train:', X_train)
     with trainCol2:
          st.write('X_test size:', len(X_test), " rows")
          st.write('X_test:', X_test)
     with trainCol3:
          st.write('y_train size:', len(y_train), " rows")
          st.write('y_train:', y_train)
     with trainCol4:
          st.write('y_test size:', len(y_test), " rows")
          st.write('y_test:', y_test)
          
     # Feature Scaling
     from sklearn.preprocessing import StandardScaler
     sc_X = StandardScaler()
     X_train = sc_X.fit_transform(X_train)
     X_test = sc_X.transform(X_test)

     # Fitting Gaussian Naive Bayes to the Training Set
     from sklearn.naive_bayes import GaussianNB
     classifier = GaussianNB()
     classifier.fit(X_train, y_train)

     # Predicting the Test set results
     y_pred = classifier.predict(X_test)

     st.subheader("3. Accuracy Scores")
     #Making the Confusion Matrix
     from sklearn.metrics import confusion_matrix, classification_report
     cm = confusion_matrix(y_test, y_pred)
     st.subheader("Confusion Matrix for Gaussian Naive Bayes")
     st.write(cm[0][0], " true negative predictions.")
     st.write(cm[1][0], " false negative predictions.")
     st.write(cm[1][0], " false positive predictions.")
     st.write(cm[1][1], " true positive predictions.")

     # Calculating Accuracy
     accuracyScore = ( cm[0][0] + cm[1][1] ) / ( len(y_test) )
     st.write("Accuracy Score:", accuracyScore)

     # Classification Report
     st.subheader("Classification Report for Gaussian Naive Bayes")
     st.write(classification_report(y_test, y_pred))

     # Predicting new values
     st.subheader("You can try to use the model to predict new values here")
     toPredict = st.text_input('Insert values of independent-variables, separate them by commas.')

     indepentVariablesValues = []
     toPredictValue = toPredict.split(",")
     for value in toPredictValue:
          indepentVariablesValues.append(int(value))
     st.write(classifier.predict([indepentVariablesValues]))
     
     # Save the model     
     if st.button('Save Model'):
          import joblib
          joblib.dump(classifier, 'saved_model_gaussian_naive_bayes.pkl')

if mlAlgorithm == 'Classification - Support Vector Machines':

     from sklearn.preprocessing import LabelEncoder
     for column in df.columns:
          if df[column].dtype == np.number: continue
          # Perform encoding for each non-numeric column
          df[column] = LabelEncoder().fit_transform(df[column])
     st.write("All the non-numeric columns have been converted to numerical data types")
     st.write(df.describe())
          
     st.subheader("1. Choosing independent and dependent variables")
     col1, col2 = st.columns(2)
     with col1:
              independentVariables = st.multiselect(
     'Which columns do you want to set as the independent values(X) ?',
     list(df.columns))
     st.write('The independent variables are:', independentVariables)
     with col2:
              dependentVariables = st.selectbox(
                  'Which column do you want to set as the dependent value(y)?',
                  list(df.columns))
              X = df[independentVariables]
              y = df[dependentVariables]

          
     st.subheader("2. Determining Size of Train Dataset")
     # importing train_test_split from sklearn
     from sklearn.model_selection import train_test_split
     # splitting the data
     testSize = st.slider(label='Set the test size here..', min_value=0.0, max_value=1.0,value=0.2)
     X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = testSize)

     trainCol1, trainCol2, trainCol3, trainCol4 = st.columns(4)
     with trainCol1:
          st.write('X_train size:', len(X_train), " rows")
          st.write('X_train:', X_train)
     with trainCol2:
          st.write('X_test size:', len(X_test), " rows")
          st.write('X_test:', X_test)
     with trainCol3:
          st.write('y_train size:', len(y_train), " rows")
          st.write('y_train:', y_train)
     with trainCol4:
          st.write('y_test size:', len(y_test), " rows")
          st.write('y_test:', y_test)
          
     # Feature Scaling
     from sklearn.preprocessing import StandardScaler
     sc_X = StandardScaler()
     X_train = sc_X.fit_transform(X_train)
     X_test = sc_X.transform(X_test)

     # Fitting Support Vector Machines to the Training Set
     from sklearn.svm import SVC  
     classifier = SVC(kernel='linear') 
     classifier.fit(X_train, y_train)

     # Predicting the Test set results
     y_pred = classifier.predict(X_test)

     st.subheader("3. Accuracy Scores")
     #Making the Confusion Matrix
     from sklearn.metrics import confusion_matrix, classification_report
     cm = confusion_matrix(y_test, y_pred)
     st.subheader("Confusion Matrix for Support Vector Machines")
     st.write(cm[0][0], " true negative predictions.")
     st.write(cm[1][0], " false negative predictions.")
     st.write(cm[1][0], " false positive predictions.")
     st.write(cm[1][1], " true positive predictions.")

     # Calculating Accuracy
     accuracyScore = ( cm[0][0] + cm[1][1] ) / ( len(y_test) )
     st.write("Accuracy Score:", accuracyScore)

     # Classification Report
     st.subheader("Classification Report for Support Vector Machines")
     st.write(classification_report(y_test, y_pred))

     # Predicting new values
     st.subheader("You can try to use the model to predict new values here")
     toPredict = st.text_input('Insert values of independent-variables, separate them by commas.')

     indepentVariablesValues = []
     toPredictValue = toPredict.split(",")
     for value in toPredictValue:
          indepentVariablesValues.append(int(value))
     st.write(classifier.predict([indepentVariablesValues]))
     
     # Save the model     
     if st.button('Save Model'):
          import joblib
          joblib.dump(classifier, 'saved_model_support_vector_machines.pkl')

if mlAlgorithm == 'Classification - K-nearest Neighbors':

     from sklearn.preprocessing import LabelEncoder
     for column in df.columns:
          if df[column].dtype == np.number: continue
          # Perform encoding for each non-numeric column
          df[column] = LabelEncoder().fit_transform(df[column])
     st.write("All the non-numeric columns have been converted to numerical data types")
     st.write(df.describe())
          
     st.subheader("1. Choosing independent and dependent variables")
     col1, col2 = st.columns(2)
     with col1:
              independentVariables = st.multiselect(
     'Which columns do you want to set as the independent values(X) ?',
     list(df.columns))
     st.write('The independent variables are:', independentVariables)
     with col2:
              dependentVariables = st.selectbox(
                  'Which column do you want to set as the dependent value(y)?',
                  list(df.columns))
              X = df[independentVariables]
              y = df[dependentVariables]

          
     st.subheader("2. Determining Size of Train Dataset")
     # importing train_test_split from sklearn
     from sklearn.model_selection import train_test_split
     # splitting the data
     testSize = st.slider(label='Set the test size here..', min_value=0.0, max_value=1.0,value=0.2)
     X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = testSize)

     trainCol1, trainCol2, trainCol3, trainCol4 = st.columns(4)
     with trainCol1:
          st.write('X_train size:', len(X_train), " rows")
          st.write('X_train:', X_train)
     with trainCol2:
          st.write('X_test size:', len(X_test), " rows")
          st.write('X_test:', X_test)
     with trainCol3:
          st.write('y_train size:', len(y_train), " rows")
          st.write('y_train:', y_train)
     with trainCol4:
          st.write('y_test size:', len(y_test), " rows")
          st.write('y_test:', y_test)
          
     # Feature Scaling
     from sklearn.preprocessing import StandardScaler
     sc_X = StandardScaler()
     X_train = sc_X.fit_transform(X_train)
     X_test = sc_X.transform(X_test)

     # Fitting K-nearest Neighbors to the Training Set
     from sklearn.neighbors import KNeighborsClassifier

     st.subheader("3. Set the number of neighbors/K to evaluate")
     kNumber = st.slider(label='Set the test size here..', min_value=1, max_value=40)
     
     classifier = KNeighborsClassifier(n_neighbors = kNumber)
     classifier.fit(X_train, y_train)

     # Predicting the Test set results
     y_pred = classifier.predict(X_test)

     st.subheader("4. Accuracy Scores")
     #Making the Confusion Matrix
     from sklearn.metrics import confusion_matrix, classification_report
     cm = confusion_matrix(y_test, y_pred)
     st.subheader("Confusion Matrix for K-nearest Neighbors")
     st.write(cm[0][0], " true negative predictions.")
     st.write(cm[1][0], " false negative predictions.")
     st.write(cm[1][0], " false positive predictions.")
     st.write(cm[1][1], " true positive predictions.")

     # Calculating Accuracy
     accuracyScore = ( cm[0][0] + cm[1][1] ) / ( len(y_test) )
     st.write("Accuracy Score:", accuracyScore)

     # Classification Report
     st.subheader("Classification Report for K-nearest Neighbors")
     st.write(classification_report(y_test, y_pred))

     # Predicting new values
     st.subheader("You can try to use the model to predict new values here")
     toPredict = st.text_input('Insert values of independent-variables, separate them by commas.')

     indepentVariablesValues = []
     toPredictValue = toPredict.split(",")
     for value in toPredictValue:
          indepentVariablesValues.append(int(value))
     st.write(classifier.predict([indepentVariablesValues]))
     
     # Save the model     
     if st.button('Save Model'):
          import joblib
          joblib.dump(classifier, 'saved_model_k_nearest_neighbors.pkl')
